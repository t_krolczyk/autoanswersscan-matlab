%tymczasowa funkcja tn�ca arkusz odpowiedzi na 4 tabelki
function [ code1, code2, answer1, answer2 ] = cutExamForm(img)
    level = graythresh(img);

    
    bw = im2bw(img, level+0.32);
    bw = 1 - bw;
    bw = bwareaopen(bw, 3);
    bw = imfilter(bw, [0 -1 0; -1 5 -1; 0 -1 0]);
    bw = imfilter(bw, [0 -1 0; -1 5 -1; 0 -1 0]);

    

    convex = bwconvhull(bw, 'objects');

    [Y, X] = size(convex);

    stats = regionprops(convex, 'Area', 'PixelIdxList', 'PixelList');
    [length, ~] = size(stats);
    regionData = zeros(length, 4);
    for i=1:length
        regionData(i, 1) = i;
        regionData(i, 2) = stats(i).Area;
        regionData(i, 3) = min(stats(i).PixelIdxList);
    end;

    [~, order] = sort(regionData(:, 2), 'descend');
    regionData = regionData(order, :);

    answers = regionData(1:2, :);
    [~, order] = sort(answers(:, 3));
    answers = answers(order, :);

    codes = regionData(3:4, :);
    [~, order] = sort(codes(:, 3));
    codes = codes(order, :);

    %obraz jest obr�cony
    AnsMinInd = min(stats(answers(1,1)).PixelList);
    CodeCardMinInd = min(stats(codes(1,1)).PixelList);
    if(AnsMinInd(1,2) < CodeCardMinInd(1,2))
        [ code1, code2, answer1, answer2 ] = cutExamForm(imrotate(img,180));
    else
        answer1 = cropImg(bw, answers(1,1), stats);
        answer2 = cropImg(bw, answers(2,1), stats);
        code1 = cropImg(bw, codes(1,1), stats);
        code2 = cropImg(bw, codes(2,1), stats);
    end


end

