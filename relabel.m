%------------------------------------------------
%funkcja sortuje indeksy w macierzy przes�anej jako argument pocz�wszy od
%lewego g�rnego rogu, od lewego obiektu do prawego
%funkcja zwraca macierz z posortowanymi indeksami
%img - macierz z odpowiedziami(tylko odpowiedzi) (!tylko jedna tabela!)
%exe - ilos� zada� 
%answ - ilos� odpowiedzi do zada�
%------------------------------------------------
function [ img, counterStartVal ] = relabel( img, exe, answ)


[~, width] = size(img);

%chodzi o to by indeks by� wi�kszy ni� max warto�� aktualna dla label,
%inaczej w algorytmie dochodzi do b��d�w
counterStartVal = max(img(:)) + 1;
counter = counterStartVal;
%liczba wszystkich mo�liwych odpowiedzi
NoA = exe * answ;
alreadyDone = zeros(NoA, 1);
[row, ~] = size(img);
cutH = round(row / exe) + 1;

%startPos - po�owa pierwszej linii odpowiedzi, cutH jest wysoko�ci� kratki
%na odpowied� + cz�� pustego miejsca mi�dzy kratkami
startPos = round(0.4*cutH);

for i = 0 : exe - 1
    line = startPos + i * cutH;
    for j = 1 : width
        if(img(line, j) ~= 0)   
            if(~(ismember(img(line, j), alreadyDone)))
                alreadyDone(counter) = img(line, j);
                alreadyDone(NoA+counter) = counter;
                [row, col] = find(img==alreadyDone(counter));
                [limitRow, ~] = size(row);
                for k = 1 : limitRow
                    img(row(k), col(k)) = counter;
                end
                counter = counter + 1;
            end
        end
    end
    %rysuje linie poziome do debugowania
    %img(line, :) = 1;
end
%imshow(img);
end

