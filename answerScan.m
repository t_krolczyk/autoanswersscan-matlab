%------------------------------------------------
%plik s�u�y do testowania poprawno�ci algorytm�w 
%wszystkie testy zawieraj� pomiar czasu wykonania
%------------------------------------------------
close all; clc; clearvars;
%macierz zawieraj�ca odpowiedzi do pierwszego zestawu testowego
correctResults = [ 1 0 0 0;   0 1 0 0;  0 0 1 0;  0 0 0 1;  0 0 0 1;  0 0 1 0;  0 1 0 0;  1 0 0 0;  1 1 0 0;  0 1 1 0;  0 0 1 1;  1 1 1 1;
    1 1 1 1;  1 1 1 0;  0 1 1 1;  1 1 0 0;  0 1 1 0;  0 0 1 1;  1 0 0 0;  1 0 0 0;  1 0 0 0;  0 1 0 0;  0 1 0 0;  0 1 0 0;  1 1 1 1];

%macierz zawieraj�ca odpowiedzi do drugiego zestawu testowego
correctResults2 = [1 0 0 0; 0 1 0 0;  0 0 1 0;  0 0 0 1; 1 1 1 1; 0 0 0 1; 0 0 1 0; 0 1 0 0; 1 0 0 0; 0 0 0 1; 0 0 1 0; 0 1 0 0; 1 0 0 0;
    1 1 0 0;  0 1 1 0;  0 0 1 1; 1 1 1 0;  0 1 1 1; 1 1 1 1; 1 0 0 0; 1 0 0 0; 0 1 0 0; 0 1 0 0; 0 0 1 0; 0 0 1 0;];

%macierz zawieraj�ca odpowiedzi do trzeciego zestawu testowego
correctResults3 = [1 0 0 0; 0 1 0 0;  0 0 1 0;  1 1 1 1; 0 0 0 1; 0 0 0 1; 0 0 1 0; 0 1 0 0; 1 0 0 0; 0 0 0 1; 0 0 1 0; 0 1 0 0; 1 0 0 0;
    1 1 0 0;  0 1 1 0;  0 0 1 1; 1 1 1 0;  0 1 1 1; 1 1 1 1; 1 0 0 0; 1 0 0 0; 0 1 0 0; 0 1 0 0; 0 0 1 0; 0 0 1 0;];

result = true;

%testowanie pojedynczego arkusza
%zmieniamy numer arkusza i zestawu testowego (3 linie poni�ej) oraz
%ustawiamy odpowiedni correctResults dla arkusza testowego
%(ps. by odkomentowa� ca�y blok dla tego testu wystarczy usun�� nawias klamrowy w linii poni�ej :D)
%
correctResults = correctResults2; % musi si� zgadza� z nr zestawu :)
tic
    img = imread('testing_set2/skan1', 'jpg');

    [codeImg1, codeImg2, answersImg1, answersImg2] = cutExamForm(img);
    [res, res2] = getAnswers(answersImg1, 25, 4);
    %tmpRes = isequal(res, res2, correctResults);
toc

%wy�wietlenie macierzy odpowiedzi z poszczeg�lnych algorytm�w
%res
%fprintf('\n\n\n');
%res2

%sprawdzenie czy ca�o�� jest poprawna 
if(~isequal(res, correctResults))
    fprintf('\nco� nie tak z indeksacj� :( \n');
elseif(~isequal(res2, correctResults))
    fprintf('\nco� nie tak z ci�ciem :( \n');
else
    fprintf('\ndobrze!\n');
end;
%}

%------------------------------------------------------------------------
%dalsze testy s� mo�liwe je�eli posiadasz pe�n� pul� arkuszy testowych
%------------------------------------------------------------------------


%testowanie wszystkich arkuszy z pierwszego zestawu testowego
%gdy kt�ry� arkusz nie przejdzie test�w jego numer wyrzucany jest na
%konsol�
%(ps. by odkomentowa� ca�y blok dla tego testu wystarczy usun�� nawias klamrowy w linii poni�ej :D)
%{
tic
for i = 1 : 27
    fileName = strcat('testing_set/skan',num2str(i));
    img = imread(fileName, 'jpg');

    [codeImg1, codeImg2, answersImg1, answersImg2] = cutExamForm(img);
    [res, res2] = getAnswers(answersImg1, 25, 4);
    tmpRes = isequal(res, res2, correctResults);
    result = result & tmpRes;
    if(~tmpRes) 
        i
        break;
    end
end

toc
if(result)
    fprintf('\ndobrze!\n');
else
    fprintf('\n�le!\n');
end;
%}


%testowanie wszystkich arkuszy z drugiego zestawu testowego
%gdy kt�ry� arkusz nie przejdzie test�w jego numer wyrzucany jest na
%konsol�
%(ps. by odkomentowa� ca�y blok dla tego testu wystarczy usun�� nawias klamrowy w linii poni�ej :D)
%{
tic
for i = 1 : 33
    fileName = strcat('testing_set2/skan',num2str(i));
    img = imread(fileName, 'jpg');

    [codeImg1, codeImg2, answersImg1, answersImg2] = cutExamForm(img);
    [res, res2] = getAnswers(answersImg1, 25, 4);
    tmpRes = isequal(res, res2, correctResults2);
    result = result & tmpRes;
    if(~tmpRes) 
        i
        break;
    end
end

toc
if(result)
    fprintf('\ndobrze!\n');
else
    fprintf('\n�le!\n');
end;
%}

%deprecated
%{
%-------wyniki--------- 
disp('|    | A | B | C | D |');
for i = 1 : HLNumber-2 
    [~, j] = max(results(i,:));
        switch j-1
            case 1
                fprintf('| %2u | X | - | - | - |\n', i);
            case 2
                fprintf('| %2u | - | X | - | - |\n', i);
            case 3
                fprintf('| %2u | - | - | X | - |\n', i);
            case 4
                fprintf('| %2u | - | - | - | X |\n', i);
        end
end
%}
