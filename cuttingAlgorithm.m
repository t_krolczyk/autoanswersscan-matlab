%------------------------------------------------
%funkcja zwraca macierz odpowiedzi (warto�ci 0, 1) bazuj�c na argumentach
%ansImg - macierz z odpowiedziami(tylko odpowiedzi) (!tylko jedna tabela!)
%exe - ilos� zada� 
%ans - ilos� odpowiedzi do zada�
%------------------------------------------------

function [ results ] = cuttingAlgorithm( ansImg, exe, answ )
results = zeros(exe, answ);
%maks wsp�czynnik dla pustego pola to xxx :D
%classyfiedAsSelectedRatio to stosunek zamalowanej przestrzeni do
%ca�kowitej prpzestrzeni dla odpowiedzi
classyfiedAsSelectedRatio = 0.12;
[row, col] = size(ansImg);

widthLines = round(col / answ) + 1;
heightLines = round(row / exe) + 1;


minX=1;
for i = 1 : exe
    for j = 0 : answ - 1 
        minY = j*widthLines+1;
        part = imcrop(ansImg,[minY, minX, widthLines, heightLines]);

        %imshow(part);
        tmpSum = sum(part(:));
        if(tmpSum > classyfiedAsSelectedRatio*size(part(:)))
            results(i,j+1)=1;
            %ansImg(minX:minX+heightLines, minY:minY+widthLines) = 1;
        end
        %rysuje linie pionowe do debugowania
        %ansImg(:,(j*widthLines) + 1)=1;
    end
    minX = i*heightLines - round(0.17*heightLines);
    %rysuje linie poziome do debugowania
    %ansImg(round(i*heightLines) - round(0.17*heightLines),:)=1;
end
%figure(2);
%imshow(ansImg);
end

