%------------------------------------------------
%funkcja jako argument przyjmuje fragment obrazu z tabel�, liczb� zada� i liczb� odp do zada� 
%zwraca macierze z wynikami skanowania poszczeg�lnymi algorytmami
%results - algorytm indeksuj�cy
%results2 - algorytm tn�cy
%------------------------------------------------

function [ results, results2 ] = getAnswers( img, exe, answ )

%obci�cie numeru odpowiedzi i numeru zadania
%dodatkow� zalet� jest mo�liwo�� usuni�cia p�niej tabeli prostym
%czysczeniem ramki(bo tabela przecina obszar ci�cia)
[height, width] = size(img);
cutWidth = round(width / (answ+2));
img = imcrop(img, [cutWidth 1 width-cutWidth height]);
cutHeight = round(height / (exe+2));
img = imcrop(img, [1 cutHeight width height - cutHeight]);
%czyszczenie szum�w
img = bwareaopen(img, 50);

%tutaj mamy 2 podej�cia, w pierwszym algorytm wykrywa du�e obszary
%danych(czyli wszystkie odpowiedzi oraz framenty ramek itp itd, po czym za
%pomoc� m. in. przekszta�ce� morfologicznych pozostawia tylko odpowiedzi

%drugi etap to czyszczenie ramki(czyli usuwa tabel�)

%nast�pnie na obrazach wykonywana jest suma logiczna
%2 kroki potrzebne s� poniewa� w przypadku przerysania ramki tabeli, taka
%odpowiedz jest usuwana razem z tabel�

%-------------------------- filtracja po��czonych obszar�w - w przypadku
%gdy kto� zamalowuj�c zamalowa� te� lini� tabelki
imFil = img;
%imshow(imFil);
%�cienianie
imFil =  bwmorph(imFil,'thin',1);

%wykrycie zaznaczonych obszar�w
skizMaskSize = round(height/230);
skizMask = ones(skizMaskSize);

imFil =  bwhitmiss(imFil,skizMask);

%wzmacniamy to co zosta�o
imFil = imdilate(imFil, strel('square',round(skizMaskSize*1.6))); 

%obliczamy �redni� z wszystkich obszar�w (pr�cz pierwszych 2 - ramka +
%buffer gdyby ramka zosta�a przerwana w jaki� spos�b)
stats = regionprops(img, 'Area');
objectAvg = mean([stats(3:end).Area]);
objectMinThreshold = round(objectAvg*1/3);
objectMaxThreshold = round(objectAvg*3);

%wykonujemy operacj� logiczn� XOR na 2 macierzach kt�re spe�niaj� warunki przekszta�cenia bwareaopen
%m�wi�c po ludzku, wynikiem b�d� wszystkie obszary o rozmiarze pomi�dzy
%objectMinThreshold i objectMaxThreshold
imFil = xor(bwareaopen(imFil,objectMinThreshold),  bwareaopen(imFil,objectMaxThreshold));


%-------------------------- filtracja po��czonych obszar�w koniec
%tutaj czy�cimy ramk�, pozostaj� na obrazie tylko puste odpowiedzi i te
%zaznaczone, kt�re nie przecinaj� ramki
answerImg = imclearborder(img, 8);
%ca�o�� zostaje delikatnie wzmocniona dylatacj�
answerImg = imdilate(answerImg, strel('square',2)); 

answerImg = answerImg + imFil;
%jeszcze raz czysczenie szumu
answerImg = bwareaopen(answerImg, round(objectAvg*1/2));
%imshow(answerImg);

%wycinamy tylko odpowiedzi
[row, col] = find(answerImg);
minC = min(col);
maxC = max(col);
minR = min(row);
maxR = max(row);
answerImg = imcrop(answerImg, [minC minR (maxC-minC) (maxR-minR)]);


%imshow(answerImg);
%------------------ algorytm tn�cy

results2 = cuttingAlgorithm(answerImg, exe, answ);

%-------------------------------algorytm indeksuj�cy

results = labelingAlgorithm(answerImg, exe, answ);

end

