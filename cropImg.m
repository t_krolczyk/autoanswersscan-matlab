function cropped = cropImg(img, areaId, stats)
    maxCords = max(stats(areaId).PixelList);
    minCords = min(stats(areaId).PixelList);
    width = maxCords(1,1) - minCords(1,1);
    height = maxCords(1,2) - minCords(1,2);
    cropped = imcrop(img, [minCords(1,1), minCords(1,2), width, height]);
end
