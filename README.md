# README #

Repozytorium dla modułu automatycznego skanera kart egzaminacyjnych odpowiedzialnego za odczytywanie odpowiedzi z arkusza.


### jak odpalić? ###

* klonujemy repozytorium
* odpalamy matlaba i otwieramy plik answerScan.m
* konfigurujemy wybrane testy i odpalamy

### pliki testowe ###

Repozytorium zawiera po jednej przykładowej karcie odpowiedzi z każdej serii testowej, dostęp do pełnej puli danych testowych posiadają odpowiednie służby:))

Wybór serii i arkusza konfigurujemy w pliku answerScan.m (w pliku znajdują się odpowiednie instrukcje)

### info o skanerze ###

Na chwilę obecną zaimplementowane zostały dwa algorytmy odczytujące odpowiedzi, w przyszłości te algorytmy + usprawnienia i kolejne algorytmy przepisane zostaną do openCV.

Dla aktualnie posiadanych danych testowych poprawność odczytu wynosi 100%. :)

### Kontakt ###

* Tomasz Królczyk 
* tom.krolczyk@gmail.com
* tkrolcz@student.agh.edu.pl