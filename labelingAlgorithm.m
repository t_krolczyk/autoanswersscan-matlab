%------------------------------------------------
%funkcja zwraca macierz odpowiedzi (warto�ci 0, 1) bazuj�c na argumentach
%ansImg - macierz z odpowiedziami(tylko odpowiedzi) (!tylko jedna tabela!)
%exe - ilos� zada� 
%ans - ilos� odpowiedzi do zada�
%------------------------------------------------

function [ results ] = labelingAlgorithm( answerImg, exe, answ )
%wzmocnienie obiekt�w(w przypadku, gdy kto� zamalowuj�c nie przekroczy�
%linii ramki do zamalowania)
answerImg = imdilate(answerImg, strel('square',3)); 
%imshow(answerImg);
%indeksowanie obiekt�w
answerImg = bwlabel(answerImg, 8);
%sortowanie indeks�w, drugi argument to numer pierwszego indeksu po
%sortowaniu
[answerImg, firstLabel] = relabel(answerImg, exe, answ); 
allAnswers = exe*answ;

stats = regionprops(answerImg, 'Area');
avg = mean([stats(:).Area]);

results = zeros(exe, answ);
for i = firstLabel : firstLabel + allAnswers - 1
    [row, ~] = find(answerImg==i);
    [m, ~] = size(row);
    if(m > (avg*1.8))
        x = fix((i-firstLabel)/4)+1;
        z = rem(i-firstLabel,4)+1;
        results(x, z) = 1;
    end
end


end

